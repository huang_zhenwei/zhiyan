

# 基于Spring Boot的线上问答系统

黄圳伟/朱晓明



## 1 基础构思

本次大作业，我们以Spring Boot框架位做后端，搭建了线上问答系统。我们定制一系列约定和规则，分别包括文件夹命名规则、文件名命名规则、程序代码编程风格、数据库设计约定等。这些规则和约定需要我们共同讨论定制，将来开发都将严格按规则或约定开发。 

系统的设计基于以下几种考虑：

1. 覆盖基本功能。系统应该满足基本的功能。
2. 后端应该尽量覆盖到每一种可能的异常，使之可以在处理异常之后继续正常运行。

## 2 系统特色

1. ### 弹性高可用，稳定高可靠

2. ### 前后端完全分离，可轻松拓展开发桌面应用程序、移动应用程序

## 3 系统功能

目前已经实现的功能有：
- [x] 用户登录/注册
- [x] 发布问题/删除问题
- [x] 发表回答/删除回答
- [x] 发表评论/删除评论



![](流程图.jpeg)

## 4 应用架构 

### 4.1 数据库设计 

后端保存问题信息、回答信息、评论信息和用户信息。

数据库的关系模式如下： 

```mysql
-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `qid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `thumb` int(11) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`aid`),
  KEY `uid` (`uid`),
  KEY `qid` (`qid`),
  CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `answers_ibfk_3` FOREIGN KEY (`qid`) REFERENCES `questions` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `thumb` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cid`),
  KEY `uid` (`uid`),
  KEY `aid` (`aid`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`aid`) REFERENCES `answers` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `qid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(255) NOT NULL,
  `title` char(80) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`qid`),
  KEY `uid` (`uid`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` char(15) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
```

### 4.2 实现逻辑

后端的文件结构是： 

```xml-dtd
├─java
│  └─edu
│      └─ptu
│          └─zhiyan
│              ├─base			#基础工具
│              ├─controller      #控制层
│              ├─entity			#实体类
│              ├─mapper			#dao层
│              └─service		#服务层
│                  ├─answer
│                  │  └─impl
│                  ├─comment
│                  │  └─impl
│                  ├─question
│                  │  └─impl
│                  └─user
│                      └─impl
└─resources
    ├─static
    └─templates

```

## 5 接口设计
### 接口说明

- API参照RESTful架构

- 每个API都会有三个表格：请求(request)参数，响应(response)参数，返回代码。其中，若响应参数的表格只有none，说明返回的data{}为空。

- API有两种请求方式：POST和GET。

  - 对于POST

    请求参数即请求示例中的data部分（向服务器发送一段json数据）

  - 对于GET

    请求参数即请求示例中的params部分（最后向服务器请求的是一个链接，类似于 http://ip:port/api?参数1=xxx&参数2=xxx）

- 请求/响应的数据的编码说明：统一使用utf-8编码

  


### 请求与响应规范

- 请求示例

  ```javascript
  POST
  {
  	url:"/api/others...",
  	method:"post",
  	headers:{
  		"Authorization":"Bearer "+token
  	},
  	data:{
  		library_name:"this is name"
  	}
  }
  
  GET
  {
  	url:"/api/others...",
  	method:"get",
  	headers:{
  		"Authorization":"Bearer "+token
  	},
  	params:{
  		library_name:"this is name"
  	}
  }
  ```

- 返回示例

  ```json
  {
  	code:200,
  	msg:"返回信息",
  	data:{
  	}
  }
  ```

- 返回代码

  - 200 ----正常返回
  
  - 400 ----出现错误
    
  - 500 ----服务器内部异常
  
    

### API详细说明

#### 基础操作

##### 登陆

```
POST	/api/login
```

| 请求参数 | 类型   | 说明   |
| -------- | ------ | ------ |
| username | String | 用户名 |
| password | String | 密码   |

| 响应参数 | 类型   | 说明   |
| -------- | ------ | :----- |
| uid      | String | 用户ID |



##### 注册

```
POST	/api/register
```

| 请求参数 | 类型   | 说明   |
| -------- | ------ | ------ |
| username | String | 用户名 |
| password | String | 密码   |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | :--- |
| none     |      |      |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 注册成功 |      |
| 400      | 错误     |      |

##### 查询用户



```
GET	/api/query
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| uid      | Integer | 用户id |

| 响应参数 | 类型   | 说明   |
| -------- | ------ | :----- |
| username | String | 用户名 |

#### 问题操作

##### 获取问题列表

```
GET	/api/question/list
```

| 请求参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 响应参数 | 类型         | 说明     |
| -------- | ------------ | -------- |
| infos    | List<Object> | 问题列表 |

| infos对象 | 类型    | 说明           |
| --------- | ------- | -------------- |
| qid       | Integer | 问题id         |
| uid       | Integer | 用户id         |
| title     | String  | 问题标题       |
| content   | String  | 问题粗略内容   |
| count     | Integer | 问题的回答人数 |
| username  | String  | 用户名         |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |

##### 获取问题具体信息

```
GET	/api/question/info
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| qid      | Integer | 问题ID |

| 响应参数  | 类型    | 说明           |
| --------- | ------- | -------------- |
| qid       | Integer | 问题id         |
| uid       | Integer | 用户id         |
| title     | String  | 问题标题       |
| content   | String  | 问题内容       |
| timestamp | Integer | 时间戳         |
| count     | Integer | 问题的回答人数 |
| username  | String  | 用户名         |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |


##### 获取指定用户的问题列表（待定）

```
GET	/api/question/list
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| uid      | Integer | 用户ID |

| 响应参数 | 类型         | 说明         |
| -------- | ------------ | ------------ |
| infos    | List<Object> | 问题列表 |

| infos对象 | 类型    | 说明     |
| --------- | ------- | -------- |
| qid       | Integer | 问题id   |
| uid       | Integer | 用户id   |
| title     | String  | 问题标题 |
| content   | String  | 问题内容 |
| timestamp | Integer | 时间戳   |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |



##### 添加问题

```
POST 	/api/question/add

headers:{
		"Authorization":"Bearer "+token
}
```

| 请求参数       | 类型          | 说明                   |
| -------------- | ------------- | ---------------------- |
| uid     | Integer       | 用户ID                 |
| title          | String        | 标题                   |
| content       | String | 问题内容           |
| timestamp     | Integer       | 时间戳               |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 添加成功 |      |
| 400      | 错误     |      |



##### 删除题目

```
POST	/api/question/delete

```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| qid      | Integer | 问题ID |
| uid      | Integer | 用户id |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 返回信息 | 说明                 |
| -------- | -------- | -------------------- |
| 200      | 删除成功 |                      |
| 400      | 删除失败 | 用户不是题目的创建人 |

#### 回答操作

##### 获取回答列表

```
GET	/api/answer/list
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| qid      | Integer | 问题id |

| 响应参数 | 类型         | 说明     |
| -------- | ------------ | -------- |
| infos    | List<Object> | 回答对象 |

| infos对象 | 类型    | 说明         |
| --------- | ------- | ------------ |
| aid       | Integer | 回答id       |
| uid       | Integer | 用户id       |
| thumb     | Integer | 点赞数       |
| content   | String  | 回答粗略内容 |
| timestamp | Integer | 时间戳       |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |

##### 获取回答具体信息

```
GET	/api/answer/info
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| aid      | Integer | 回答ID |

| infos对象 | 类型    | 说明       |
| --------- | ------- | ---------- |
| aid       | Integer | 回答id       |
| uid       | Integer | 用户id       |
| thumb     | Integer | 点赞数       |
| content   | String  | 回答内容 |
| timestamp | Integer | 时间戳       |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |



##### 添加回答

```
POST 	/api/answer/add
```

| 请求参数  | 类型    | 说明     |
| --------- | ------- | -------- |
| uid       | Integer | 用户id   |
| qid       | Integer | 问题id   |
| content   | String  | 回答内容 |
| timestamp | Integer | 时间戳   |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 添加成功 |      |
| 400      | 错误     |      |



##### 删除回答

```
POST	/api/answer/delete

```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| qid      | Integer | 问题ID |
| uid      | Integer | 用户id |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 返回信息 | 说明             |
| -------- | -------- | ---------------- |
| 200      | 删除成功 |                  |
| 400      | 删除失败 | 用户不是回答的人 |

#### 评论操作
##### 获取评论列表

```
GET	/api/comment/list
```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| aid      | Integer | 回答id |

| 响应参数 | 类型         | 说明     |
| -------- | ------------ | -------- |
| infos    | List<Object> | 回答数组 |

| infos对象 | 类型    | 说明     |
| --------- | ------- | -------- |
| aid       | Integer | 回答id   |
| uid       | Integer | 用户id   |
| thumb     | Integer | 点赞数   |
| content   | String  | 评论内容 |
| timestamp | Integer | 时间戳   |
| cid       | Integer | 评论id   |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 获取成功 |      |
| 400      | 错误     |      |



##### 添加评论

```
POST 	/api/comment/add
```

| 请求参数  | 类型    | 说明     |
| --------- | ------- | -------- |
| uid       | Integer | 用户id   |
| aid       | Integer | 回答id   |
| content   | String  | 回答内容 |
| timestamp | Integer | 时间戳   |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 提示信息 | 说明 |
| -------- | -------- | ---- |
| 200      | 添加成功 |      |
| 400      | 错误     |      |



##### 删除评论

```
POST	/api/comment/delete

```

| 请求参数 | 类型    | 说明   |
| -------- | ------- | ------ |
| qid      | Integer | 问题ID |
| uid      | Integer | 用户id |

| 响应参数 | 类型 | 说明 |
| -------- | ---- | ---- |
| none     |      |      |

| 返回代码 | 返回信息 | 说明             |
| -------- | -------- | ---------------- |
| 200      | 删除成功 |                  |
| 400      | 删除失败 | 用户不是回答的人 |



## 6 总结 

### 应用设计的优点：

1. 代码上，模块分工清晰。
2. 功能比较完整。
3. 覆盖各种可能异常。

### 未来的工作

1. 优化数据库查询，尽量把冗余降低。
2. 优化数据库模式。


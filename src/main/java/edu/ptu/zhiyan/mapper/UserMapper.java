package edu.ptu.zhiyan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.ptu.zhiyan.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description 用户信息的mapper
 */
// 仅供自己理解：BaseMapper封装了对crud的增删改查，故我们只需要直接调用
@Mapper
public interface UserMapper extends BaseMapper<User> {

}

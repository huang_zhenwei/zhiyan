package edu.ptu.zhiyan.service.answer;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.ptu.zhiyan.entity.Answer;

/**
 *  @author shengxiao
 *  @date 2021/5/21 23:07
 */
public interface IAnswerService extends IService<Answer>  {
}

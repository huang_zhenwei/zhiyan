package edu.ptu.zhiyan.service.answer.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import com.baomidou.mybatisplus.core.conditions.update.Update;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.ptu.zhiyan.entity.Answer;
import edu.ptu.zhiyan.entity.Question;
import edu.ptu.zhiyan.mapper.AnswerMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author shengxiao
 * @date 2021/5/21 23:24
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer> implements IService<Answer> {

    @Resource
    AnswerMapper answerMapper ;

    // 添加回答
    public boolean saveAnswer(Answer answer){

        answer.setTimestamp(new Date());
        answer.setThumb(0);
        return answerMapper.insert(answer) == 1 ? true : false ;
    }

    // 获取回答列表
    public List<Answer> queryAnswer(Integer qid){

        List<Answer> retList = answerMapper.selectList(new QueryWrapper<Answer>().eq(Answer.QID, qid));
//
//        for(int i = 0 ; i < retList.size() ; i++){
//            Answer tmp = retList.get(i) ;
//            int length = tmp.getContent().length() ;
//            if(length < Answer.MAXCONTENT){
//                continue;
//            }else{
//                String substr = tmp.getContent().substring(0, Answer.MAXCONTENT) ;
//                tmp.setContent(substr);
//                //System.out.println(tmp.getContent());
//                //res.add(questionInfo) ;
//            }
//        }
        return retList ;
    }

    // 获取回答具体信息
    public Answer queryDetailAnswer(Integer aid){

        return answerMapper.selectOne(new QueryWrapper<Answer>().eq(Answer.AID, aid));
    }

    // 删除回答
    public boolean deleteAnswer(Integer qid,Integer uid){

        Integer result = answerMapper.delete(new QueryWrapper<Answer>().eq(Answer.QID, qid)
                                                              .eq(Answer.UID, uid)) ;
        return result == 1 ? true : false ;
    }

    // 添加点赞
    public boolean insertAThumb(Integer aid){

//        QueryWrapper<Answer> queryWrapper = new QueryWrapper<>() ;
//        String sql = "select thumb from answer where aid = "
//        queryWrapper.exists;
        // 自定义sql语句的更新方式

        QueryWrapper<Answer> queryWrapper = new QueryWrapper<>() ;
        queryWrapper.eq(Answer.AID, aid) ;
        Answer answer = answerMapper.selectOne(queryWrapper) ;

        answer.setThumb(answer.getThumb() + 1);
        UpdateWrapper<Answer> updateWrapper = new UpdateWrapper<>() ;
        updateWrapper.eq(Answer.AID,aid) ;
        answerMapper.update(answer,updateWrapper) ;

        return true ;
    }
}

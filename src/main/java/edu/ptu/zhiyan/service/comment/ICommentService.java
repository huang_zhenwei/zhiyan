package edu.ptu.zhiyan.service.comment;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.ptu.zhiyan.entity.Comment;

/**
 * @author shengxiao
 * @date 2021/5/21 23:31
 */
public interface ICommentService extends IService<Comment> {
}

package edu.ptu.zhiyan.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.ptu.zhiyan.entity.User;

// 通过继承 mybatis  plus，mp，从而实现了简化crud。即需要让此接口继承IService，然后IService接口的泛型为User类，
// 即 要通过service->mapper->访问数据库【操作表】-->对应的实体类，故泛型需为User类
public interface IUserService extends IService<User> {
}

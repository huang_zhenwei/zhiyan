package edu.ptu.zhiyan.service.question;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.ptu.zhiyan.entity.Question;

/**
 * @author shengxiao
 * @date 2021/5/21 23:28
 */
public interface IQuestionService extends IService<Question> {
}

package edu.ptu.zhiyan.service.question.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.deser.DataFormatReaders;
import edu.ptu.zhiyan.entity.Answer;
import edu.ptu.zhiyan.entity.Question;
import edu.ptu.zhiyan.entity.User;
import edu.ptu.zhiyan.mapper.AnswerMapper;
import edu.ptu.zhiyan.mapper.QuestionMapper;
import edu.ptu.zhiyan.service.user.impl.UserServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author shengxiao
 * @date 2021/5/21 23:30
 */
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IService<Question> {

    @Resource
    QuestionMapper questionMapper ;

    @Resource
    UserServiceImpl userServiceImpl;    // 为了获取用户名，需要注入userServiceImpl

    @Resource
    AnswerMapper answerMapper ; // 为了获取问题回答人数，此处要注入
    // 添加问题
    public boolean addQuestion(Question question){
        try {
            question.setTimestamp(new Date());
            int result = questionMapper.insert(question) ;
            System.out.println("result" + result);
            return result > 0 ? true : false ;
        } catch (Exception e) {
            e.printStackTrace();
            return false ;
        }
    }

    // 查询问题列表【缩略信息】
    // 注意：为了获取count字段和username字段，故只能新创建一个实体类来接收所要返回的信息
    // 要充分利用之前写好的方法，结合其他service层的的方法进行简化开发。
    public List<QuestionInfo> queryQuestion(){

        try {
            List<Question> questionList = questionMapper.selectList(new QueryWrapper<>());
            List<QuestionInfo> res = new ArrayList<>();     // 包含内部类的列表，用来 接收信息
            for (int i=0;i<questionList.size();i++) {
                Question tmp=questionList.get(i);
                QuestionInfo questionInfo = new QuestionInfo(tmp);
                questionInfo.setUsername(userServiceImpl.queryUser(tmp.getUid()).getUsername());
                Integer cnt = answerMapper.selectCount(new QueryWrapper<Answer>().eq(Answer.QID,tmp.getQid()));   // 问题的回答人数
                questionInfo.setCount(cnt);

                int length = tmp.getContent().length() ;
                //System.out.println("length：" + length);
                if(length < Question.MAXCONTENT){
                    res.add(questionInfo);  // 将处理好的info对象存放在list列表中
                    continue ;
                }else{
                    // 注意：substring是非原地操作的
                    //String subStr = questionList.get(i).getContent().substring(0, Question.MAXCONTENT) ;
                    //questionList.get(i).setContent(subStr);
                    String substr = tmp.getContent().substring(0,Question.MAXCONTENT) ;
                    questionInfo.setContent(substr);
                    System.out.println(tmp.getContent());
                    res.add(questionInfo) ;
                }

            }

            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null ;
        }
    }


    // 获取问题详细信息【就针对一个问题的详细信息】
    public QuestionInfo queryDetailQuestion(Integer qid){

        try {
            Question question = questionMapper.selectOne(new QueryWrapper<Question>().eq(Question.QID,qid));
            QuestionInfo res = new QuestionInfo(question) ;
            /*for (int i=0;i<questionList.size();i++) {
                Question tmp=questionList.get(i);
                QuestionInfo questionInfo = new QuestionInfo(tmp);
                questionInfo.setUsername(userServiceImpl.queryUser(tmp.getUid()).getUsername());
                Integer cnt = answerMapperMapper.selectCount(new QueryWrapper<Answer>().eq(Answer.QID,tmp.getQid()));
                questionInfo.setCount(cnt);
                res.add(questionInfo);
            }*/
            //res.setCount();

            res.setCount(answerMapper.selectCount(new QueryWrapper<Answer>().eq(Answer.QID,question.getQid()))) ;
            res.setUsername(userServiceImpl.queryUser(question.getUid()).getUsername());
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null ;
        }
    }

    // 删除问题
    public boolean removeQuestion(Integer qid,Integer uid){

        try {
            QueryWrapper<Question> queryWrapper = new QueryWrapper<>() ;
            queryWrapper.eq(Question.QID, qid)
                        .eq(Question.UID, uid) ;
            Integer result = questionMapper.delete(queryWrapper) ;
            System.out.println(result);
            return result > 0 ? true : false ;

        } catch (Exception e) {
            e.printStackTrace();
            return false ;
        }

    }

    // 内部类，为了键入username和count字段
    public class QuestionInfo{
        private String username;
        private Integer count;
        private Integer qid;
        private Integer uid;
        private String title;
        private String content;
        private Date timestamp;
        public QuestionInfo(Question q){
            qid=q.getQid();
            uid=q.getUid();
            title=q.getTitle();
            content=q.getContent();
            timestamp=q.getTimestamp();
        }
        public QuestionInfo(){};

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Integer getQid() {
            return qid;
        }

        public void setQid(Integer qid) {
            this.qid = qid;
        }

        public Integer getUid() {
            return uid;
        }

        public void setUid(Integer uid) {
            this.uid = uid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }
    }
}

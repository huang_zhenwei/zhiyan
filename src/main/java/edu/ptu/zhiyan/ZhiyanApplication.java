package edu.ptu.zhiyan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

// 取消springboot自带的鉴权功能，未使用到过滤器，监听器
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class ZhiyanApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZhiyanApplication.class, args);
    }

}

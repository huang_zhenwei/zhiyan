package edu.ptu.zhiyan.controller;

import com.alibaba.fastjson.JSONObject;
import edu.ptu.zhiyan.base.Result;
import edu.ptu.zhiyan.entity.Answer;
import edu.ptu.zhiyan.service.answer.impl.AnswerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shengxiao
 * @date 2021/5/21 23:03
 */

@RestController
//@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/api/answer")
public class AnswerController {

    @Resource
    AnswerServiceImpl answerServiceImpl ;

    // 获取回答列表
    @GetMapping("/list")
    public Result obtainAnswer(@RequestParam("qid") Integer qid){

        List<Answer> retList = answerServiceImpl.queryAnswer(qid);
        //return  null ;
        if(retList != null){
            return Result.ok(retList, "获取回答列表成功") ;
        }else{
            return Result.fail("获取回答列表失败") ;
        }
    }

    // 添加回答
    @PostMapping("/add")
    public Result addAnswer(@RequestBody Answer answer){

        if(answerServiceImpl.saveAnswer(answer)){
            return Result.ok("添加回答成功") ;
        }else{
            return Result.ok("添加回答失败") ;
        }
    }

    // 获取指定问题答案详细信息
    @GetMapping("/info")
    public Result showAnswer(@RequestParam("aid") Integer aid){

        Answer answer = answerServiceImpl.queryDetailAnswer(aid) ;
        if(answer != null){
            return Result.ok(answer,"获取答案详细信息成功") ;
        }else{
            return Result.fail("获取答案详细信息失败") ;
        }
    }

    // 删除回答
    @PostMapping("/delete")
    public Result removeAnswer(@RequestBody JSONObject jsonObject){

        if(answerServiceImpl.deleteAnswer(jsonObject.getIntValue("aid"), jsonObject.getIntValue("uid"))) {
            return Result.ok("删除回答成功") ;
        }else{
            return Result.fail("删除回答失败") ;
        }
    }


    // 添加点赞
    /**
     * 要给问题的回答添加点赞，以及评论添加点赞
     */
    @PostMapping("/click")
    public Result addThump(@RequestBody JSONObject jsonObject){

        if(answerServiceImpl.insertAThumb(jsonObject.getIntValue("aid"))){
            return Result.ok("点赞成功") ;
        }else{
            return Result.fail("点赞失败") ;
        }
    }
}

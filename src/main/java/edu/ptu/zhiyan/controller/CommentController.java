package edu.ptu.zhiyan.controller;

import com.alibaba.fastjson.JSONObject;
import edu.ptu.zhiyan.base.Result;
import edu.ptu.zhiyan.entity.Comment;
import edu.ptu.zhiyan.service.comment.impl.CommentServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shengxiao
 * @date 2021/5/21 23:40
 */

@RestController
//@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/api/comment")
public class CommentController {

    @Resource
    private CommentServiceImpl commentServiceImpl ;

    // 添加评论
    @PostMapping("/add")
    public Result addComment(@RequestBody Comment comment){

        if(commentServiceImpl.saveComment(comment)){
            return Result.ok("添加评论成功") ;
        }else{
            return Result.fail("添加评论失败") ;
        }
    }

    // 获取评论列表
    @GetMapping("/list")
    public Result obtainComment(@RequestParam("aid") Integer aid){

        List<Comment> queryComments = commentServiceImpl.queryComments(aid);
        if(queryComments != null){
            return Result.ok(queryComments, "获取评论列表成功") ;
        }else{
            return Result.fail("获取评论列表失败") ;
        }
    }

    // 删除评论
    @PostMapping("/delete")
    public Result removeComment(@RequestBody JSONObject jsonObject){

        if(commentServiceImpl.deleteComment(jsonObject.getIntValue("qid"), jsonObject.getIntValue("uid"))) {
            return Result.ok("删除评论成功") ;
        }else {
            return Result.fail("删除评论失败") ;
        }
    }

    // 添加评论点赞
    @PostMapping("/click")
    public Result addThump(@RequestBody JSONObject jsonObject){

        if(commentServiceImpl.insertCThumb(jsonObject.getIntValue("cid"))){
            return Result.ok("点赞成功") ;
        }else{
            return Result.fail("点赞失败") ;
        }
    }
}

